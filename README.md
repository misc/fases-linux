# fases+linux

*A proof-of-concept software distribution using the fases coreutils and shell.*

![](./Screenshot.png)

## How to compile

* Acquire the linux or linux-libre kernel ;

* Configure the kernel, you have to do at least the following selections ;

```
Kernel Configuration
	[*] 64-bit kernel # Will change soon.
	General Setup --->
		Default init path
			/init
		[*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
			Initramfs source file(s)
				../rootfs/
	Executable file formats --->
		[*] Kernel support for ELF binaries
		[*] Kernel supports for scripts starting with #!
	Device Drivers --->
		Character devices --->
			[*] Enable TTY
```

* Configure permissions ;

```
$ chmod +x rootfs/bin/*
```

* Build it ;

```
# Be sure that you're in the kernel directory.
$ make
```

* The image is located at `arch/x86/boot/bzImage`.

Note: The image is **not** a disk image, you can't boot it directly. Use 
a bootloader to load it. You can also use the following command to boot 
it in a QEMU Virtual Machine:

```
$ qemu-system-x86_64 -kernel arch/x86/boot/bzImage <QEMU arguments>
```

## Size

As of Saturday 10th of September 2022:

```
-rw-r--r-- 1 thelinuxmacbook thelinuxmacbook 1717488 Sep 10 15:45 linux/arch/x86/boot/bzImage
```

## Do not use this as your main OS!

This software distribution is a **proof-of-concept**, it isn't made to be 
usable as an Operating System but rather as a testing environnement.

Anyways, why would you use this as your main OS? Networking doesn't even work!
